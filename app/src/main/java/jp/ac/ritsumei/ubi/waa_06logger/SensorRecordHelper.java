package jp.ac.ritsumei.ubi.waa_06logger;

/**
 * Created by nokkii on 15/09/23.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;

public class SensorRecordHelper extends SQLiteOpenHelper {
    final static private int DB_VERSION = 1;

        public SensorRecordHelper(Context context, File file) {
            super(context, file.getPath(), null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // table create
            db.execSQL(
                    "CREATE TABLE sensor_datas(" +
                            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                            "gx REAL," +
                            "gy REAL," +
                            "gz REAL," +
                            "gyx REAL," +
                            "gyy REAL," +
                            "gyz REAL," +
                            "latitude REAL," +
                            "longitude REAL," +
                            "created_at datetime," +
                            "kmh REAL," +
                            "image BLOB)"
            );
            db.execSQL(
                    "CREATE TABLE time_sprits(" +
                            "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE," +
                            "created_at datetime )"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // データベースの変更が生じた場合は、ここに処理を記述する。
        }
}
