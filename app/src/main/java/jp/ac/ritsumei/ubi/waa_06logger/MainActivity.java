package jp.ac.ritsumei.ubi.waa_06logger;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.location.Location;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import java.nio.ByteBuffer;

import android.graphics.Bitmap;
import android.media.Image;
import android.media.ImageReader;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class MainActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient. OnConnectionFailedListener,
        LocationListener {


    BluetoothSPP bt;
    TextView textStatus, textRead;
    EditText etMessage;
    Boolean isInit = false;
    ArrayList<SensorData> sensorDatas = new ArrayList<>();
    Button btnSend, btnRecImage;
    Menu menu;

    // LocationClient の代わりにGoogleApiClientを使います
    private GoogleApiClient mGoogleApiClient;
    private final int REQUEST_PERMISSION = 10;
    private boolean mResolvingError = false;
    private FusedLocationProviderApi fusedLocationProviderApi;
    private LocationRequest locationRequest;
    private Location location;
    private long lastLocationTime = 0;

    SimpleDateFormat mTimeFormat = new SimpleDateFormat("hhmmssSSS", Locale.JAPAN);
    Timer mTimer = new Timer(true);
    char mTimerCounter = 0;
    Handler mHandler = new Handler();
    String columns[] = {"gx", "gy", "gz", "gyx", "gyy", "gyz", "latitude", "longitude", "kmh", "createdAt"};
    SensorRecordHelper dbHelper;
    SQLiteDatabase db;

    static final String DB = "sqlite_sample.db";
    static final int DB_VERSION = 1;
    static final String CREATE_TABLE = "create table mytable ( _id integer primary key autoincrement, data integer not null );";
    static final String DROP_TABLE = "drop table mytable;";

    // カメラ
    private AutoFitTextureView mTextureView;
    private ImageView mImageView;
    private Camera2StateMachine mCamera2;

    // キャプチャ
    private static final String TAG = "ScreenCapture";
    private static final int REQUEST_CODE_SCREEN_CAPTURE = 1;
    private boolean isImageShot =false;
    private boolean isImageRecord =false;

    PowerManager.WakeLock lock;

    private MediaProjectionManager mMediaProjectionManager;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;

    private ImageReader mImageReader;
    private int mWidth;
    private int mHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Check", "onCreate");

        File file = new File(getExternalFilesDir("").getPath() + "/sensor_datas.db");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dbHelper = new SensorRecordHelper(this, file);
        db = dbHelper.getWritableDatabase();
        textRead = (TextView)findViewById(R.id.textRead);
        textStatus = (TextView)findViewById(R.id.textStatus);
        etMessage = (EditText)findViewById(R.id.etMessage);
        textRead.setBackgroundColor(Color.WHITE);

        if(Build.VERSION.SDK_INT >= 23){
            checkPermission();
        }
        else{
            locationInit();
        }

        //画面ON
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My tag");
        lock.acquire();

        //camera
        mTextureView = (AutoFitTextureView) findViewById(R.id.TextureView);
        mImageView = (ImageView) findViewById(R.id.ImageView);
        mCamera2 = new Camera2StateMachine();

        // Lintによるサジェスト "Must be one of..."が赤い下線で出るケースがあります。
        // ここでは無視します（ビルドできる）
        mMediaProjectionManager = (MediaProjectionManager)
                getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        // MediaProjectionの利用にはパーミッションが必要。
        // ユーザーへの問い合わせのため、Intentを発行
        Intent permissionIntent = mMediaProjectionManager.createScreenCaptureIntent();
        startActivityForResult(permissionIntent, REQUEST_CODE_SCREEN_CAPTURE);
        bt = new BluetoothSPP(this);

        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }

        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                if(-1 != message.indexOf("ags")) {
                    String[] senseData = message.split(",");
                    SensorData sesorData = new SensorData();
                    ByteArrayOutputStream bos = null;

                    sesorData.gx  = Integer.parseInt(senseData[3]);
                    sesorData.gy  = Integer.parseInt(senseData[4]);
                    sesorData.gz  = Integer.parseInt(senseData[5]);
                    sesorData.gyx = Integer.parseInt(senseData[6]);
                    sesorData.gyy = Integer.parseInt(senseData[7]);
                    sesorData.gyz = Integer.parseInt(senseData[8]);
                    sesorData.latitude = location.getLatitude();
                    sesorData.longitude = location.getLongitude();
                    sesorData.createdAt = senseData[2];
                    sesorData.kmh = (location.getSpeed() * 60f * 60f) / 1000f;

                    //ss
                    if (isImageShot && isImageRecord) {
                        bos = new ByteArrayOutputStream();
                        getScreenshot().compress(Bitmap.CompressFormat.JPEG, 60, bos);
                        isImageShot = false;
                    }
                    sesorData.image = (null == bos ? null : bos.toByteArray());

                    sensorDatas.add(sesorData);

                    textRead.setText(
                            message + "\n" +
                                    "Timestamp:" + senseData[2] + "\n" +
                                    "GX:" + senseData[3] + "\n" +
                                    "GY:" + senseData[4] + "\n" +
                                    "GZ:" + senseData[5] + "\n" +
                                    "GYX:" + senseData[6] + "\n" +
                                    "GYY:" + senseData[7] + "\n" +
                                    "GYZ:" + senseData[8] + "\n" +
                                    "LocationMode:" + location.getProvider() + "\n" +
                                    "Latitude:" + sesorData.latitude + "\n" +
                                    "Longitude:" + sesorData.longitude + "\n" +
                                    "km/h:" + sesorData.kmh + "\n"
                    );
                }else {
                    textRead.append(message + "\n");
                }
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceDisconnected() {
                textStatus.setText("Status : Not connect");
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_connection, menu);
            }

            public void onDeviceConnectionFailed() {
                textStatus.setText("Status : Connection failed");
            }

            public void onDeviceConnected(String name, String address) {
                textStatus.setText("Status : Connected to " + name);
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_disconnection, menu);
            }
        });

        mTimer.schedule( new TimerTask(){
            @Override
            public void run() {
                boolean post = mHandler.post(new Runnable() {
                    public void run() {
                        mTimerCounter++;
                        isImageShot = true;
                        if (50 <= mTimerCounter) {
                            ArrayList<SensorData> datas = sensorDatas;
                            sensorDatas = new ArrayList<>();
                            insertIntoDBWithTransaction(datas);
                            mTimerCounter = 0;
                        }
                        if (!isInit && location != null) {
                            textRead.setText(
                                    "LocationMode:" + location.getProvider() + "\n" +
                                            "Latitude:" + location.getLatitude() + "\n" +
                                            "Longitude:" + location.getLongitude() + "\n" +
                                            "km/h:" + ((location.getSpeed() * 60f * 60f) / 1000f) + "\n" +
                                            "time:" + (mTimeFormat.format(new Date())) + "\n"
                            );
                        }
                    }
                });
            }
        }, 100, 100);
    }

    private void insertIntoDBWithTransaction(ArrayList<SensorData> datas){
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        db.beginTransaction();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ", Locale.JAPAN);
            String today = dateFormat.format(new Date());
            ContentValues values = new ContentValues();
            for(SensorData sensorData: datas){
                values.put("gx", sensorData.gx);
                values.put("gy", sensorData.gy);
                values.put("gz", sensorData.gz);
                values.put("gyx", sensorData.gyx);
                values.put("gyy", sensorData.gyy);
                values.put("gyz", sensorData.gyz);
                values.put("latitude", sensorData.latitude);
                values.put("longitude", sensorData.longitude);
                values.put("created_at",
                        today +
                                sensorData.createdAt.substring(0, 2) + ":" +
                                sensorData.createdAt.substring(2, 4) + ":" +
                                sensorData.createdAt.substring(4, 6) + "." +
                                sensorData.createdAt.substring(6, 9)
                );
                values.put("kmh", sensorData.kmh);
                values.put("image", sensorData.image);
                db.insert("sensor_datas", null, values);
                values.clear();
            }
            db.setTransactionSuccessful();
            Log.d("db", "reccorded!");
        //finally {
            db.endTransaction();
            db.close();
        //}
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_connection, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_android_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_ANDROID);
			/*
			if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
    			bt.disconnect();*/
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if(id == R.id.menu_device_connect) {
            bt.setDeviceTarget(BluetoothState.DEVICE_OTHER);
			/*
			if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
    			bt.disconnect();*/
            Intent intent = new Intent(getApplicationContext(), DeviceList.class);
            startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
        } else if(id == R.id.menu_disconnect) {
            if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
                bt.disconnect();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }

    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
                setup();
            }
        }

        // Connect the client.
        if (!mResolvingError) {
            // Connect the client.
            mGoogleApiClient.connect();
        }
    }

    public void setup() {
        btnSend = (Button)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (isInit) {
                    if (etMessage.getText().length() != 0) {
                        bt.send(etMessage.getText().toString(), true);
                        etMessage.setText("");
                    }
                } else {
                    sensorinit();
                    isInit = true;
                    btnSend.setText("Record");
                }
            }
        });
        btnRecImage = (Button) findViewById(R.id.btnRecImage);
        btnRecImage.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                isImageRecord = !isImageRecord;
                if(isImageRecord) {
                    btnRecImage.setText("Image/OFF");
                    textRead.setVisibility(View.GONE);
                } else {
                    btnRecImage.setText("Image/ON");
                    textRead.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    public void sensorinit() {
        SimpleDateFormat timeFormat = new SimpleDateFormat("hhmmssSSS", Locale.JAPAN);
        Date date = new Date();
        String message = "sett " + timeFormat.format(date) + "\r\n"; //hhmmssSSS
        bt.send(message.toString(), true);
        //加速度計測を行うコマンド文字列を作成
        //加速度を、2秒後から、20msec単位で、1回平均化、10データ送信
        //strcpy(szCommand,"sens +000002000 20 1 10\r\n");
        message = "ags +000002000 10 1 00\r\n";
        bt.send(message.toString(), true);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_ANDROID);
                setup();
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }

        if (REQUEST_CODE_SCREEN_CAPTURE == requestCode) {
            if (resultCode != RESULT_OK) {
                //パーミッションなし
                Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                return;
            }
            // MediaProjectionの取得
            mMediaProjection =
                    mMediaProjectionManager.getMediaProjection(resultCode, data);

            DisplayMetrics metrics = getResources().getDisplayMetrics();
            //mWidth = metrics.widthPixels;
            //mHeight = metrics.heightPixels;
            mWidth = 360;
            mHeight = 640;
            int density = metrics.densityDpi;

            Log.d(TAG,"setup VirtualDisplay");
            mImageReader = ImageReader
                    .newInstance(mWidth, mHeight, ImageFormat.RGB_565, 2);
            mVirtualDisplay = mMediaProjection
                    .createVirtualDisplay("Capturing Display",
                            mWidth, mHeight, density,
                            DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                            mImageReader.getSurface(), null, null);
        }
    }

    @Override
    protected void onResume() {
        mCamera2.open(this, mTextureView);
        super.onResume();
    }

    @Override
    protected void onPause() {
        // 他画面へ遷移する場合は位置情報取得を止めます。
        //simpleLocationManager.stop();
        super.onPause();
        mCamera2.close();
    }

    private Bitmap getScreenshot() {
        // ImageReaderから画面を取り出す
        Image image = mImageReader.acquireLatestImage();
        Image.Plane[] planes = image.getPlanes();
        ByteBuffer buffer = planes[0].getBuffer();

        int pixelStride = planes[0].getPixelStride();
        int rowStride = planes[0].getRowStride();
        int rowPadding = rowStride - pixelStride * mWidth;

        // バッファからBitmapを生成
        Bitmap bitmap = Bitmap.createBitmap(
                mWidth + rowPadding / pixelStride, mHeight,
                Bitmap.Config.RGB_565);
        bitmap.copyPixelsFromBuffer(buffer);
        image.close();

        return bitmap;
    }

    // 位置情報許可の確認
    public void checkPermission() {
        // 既に許可している
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            locationInit();
        }
        // 拒否していた場合
        else{
            requestLocationPermission();
        }
    }

    // 許可を求める
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);

        } else {
            Toast toast = Toast.makeText(this, "許可されないとアプリが実行できません", Toast.LENGTH_SHORT);
            toast.show();

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, REQUEST_PERMISSION);

        }
    }

    // 結果の受け取り
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            // 使用が許可された
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationInit();
                return;

            } else {
                // それでも拒否された時の対応
                Toast toast = Toast.makeText(this, "これ以上なにもできません", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    public void locationInit(){
        // LocationRequest を生成して精度、インターバルを設定
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(500);
        locationRequest.setFastestInterval(16);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (!mResolvingError) {
            // Connect the client.
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        fusedLocationProviderApi.removeLocationUpdates(mGoogleApiClient, MainActivity.this);
        mGoogleApiClient.disconnect();
        lock.release();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("LocationActivity", "onConnected");

        Location currentLocation = fusedLocationProviderApi.getLastLocation(mGoogleApiClient);
        location = currentLocation;
        fusedLocationProviderApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location_) {
        lastLocationTime = location.getTime() - lastLocationTime;
        Log.d("location", "lat:"+location_.getLatitude()+" lon:"+location_.getLongitude());
        location = location_;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (mResolvingError) {
            return;
        } else if (connectionResult.hasResolution()) {

        } else {
            mResolvingError = true;
        }
    }
}
