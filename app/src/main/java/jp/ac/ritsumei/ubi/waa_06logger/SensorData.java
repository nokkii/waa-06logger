package jp.ac.ritsumei.ubi.waa_06logger;


import android.graphics.Bitmap;

/**
 * Created by nokkii on 15/08/27.
 */
/*
@DataModel(find = {
        "id", "gx", "gy", "gz", "gyx", "gyy", "gyz", "latitude", "longitude", "created_at:createdAt"
}, unique = {
        "id"
})
*/
public class SensorData {
    /*
    public enum Role {
        PROGRAMMER, DESIGNNER, MANAGER
    }
    */
    public Long id;
    public int gx;
    public int gy;
    public int gz;
    public int gyx;
    public int gyy;
    public int gyz;
    public Double latitude;
    public Double longitude;
    public float kmh;
    public byte[] image;
    public String createdAt;
    /* GetterとSetterは省略 */
}
